import CalculatorHeaderComponent from "../components/calculatorHeader.component.js";
import CalculatorFormComponent from "../components/calculatorForm.component.js";
import CalculatorResultBlock from "../components/calculatorResultBlock.component.js";

export default class PricingCalculatorPage {

    constructor() {
        this.calculatorHeader = new CalculatorHeaderComponent();
        this.calculatorForm = new CalculatorFormComponent();
        this.calculatorResultBlock = new CalculatorResultBlock();
    }
}