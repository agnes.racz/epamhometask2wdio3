import HeaderComponent from "../components/header.component.js";


export default class BasePage {

    constructor() {
        this.header = new HeaderComponent();
    }

    open() {
        return browser.url("https://cloud.google.com/?hl=en");
    }
}