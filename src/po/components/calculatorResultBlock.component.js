import BaseComponent from "./base.component.js";

export default class CalculatorResultBlock extends BaseComponent {
    constructor() {
        super("#resultBlock");
    }

    output(name) {
        const selectors = {
            noOfInstances: "div > div.md-no-sticky.md-subheader._md > div > div > span",
            datacenterLocation: "md-list-item:nth-child(2) > div.md-list-item-text.ng-binding",
            usage: "md-list-item:nth-child(6) > div.md-list-item-text.ng-binding",
            model: "md-list-item:nth-child(8) > div.md-list-item-text.ng-binding",
            machineType: "md-list-item:nth-child(10)",
            os: "md-list-item:nth-child(12) > div.md-list-item-text.cpc-cart-multiline.flex > span",
            gpuType: "md-list-item:nth-child(14) > div.md-list-item-text.ng-binding.cpc-cart-multiline.flex",
            ssd: "md-list-item:nth-child(16) > div.md-list-item-text.ng-binding.cpc-cart-multiline.flex"
        }
        return this.rootEl.$(selectors[name])
    }

    get cartTotal() {
        return $(".cpc-cart-total");
    }

}