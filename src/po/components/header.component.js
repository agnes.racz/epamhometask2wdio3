import BaseComponent from "./base.component.js";

export default class HeaderComponent extends BaseComponent {
    constructor() {
        super(".TDbJKc");
    }

    get searchBtn(){
        return this.rootEl.$("[aria-label='Search']");
    }

}