import BaseComponent from "./base.component.js";

export default class CalculatorFormComponent extends BaseComponent {
    constructor() {
        super("[name='ComputeEngineForm']");
    }

    input(name) {
        const selectors = {
            noOfInstances: "[ng-model='listingCtrl.computeServer.quantity']",
            series: "[ng-model='listingCtrl.computeServer.series']",
            machineType: "[ng-model='listingCtrl.computeServer.instance']",
            gpuType: "[ng-model='listingCtrl.computeServer.gpuType']",
            ssd: "[ng-model='listingCtrl.computeServer.ssd']",
            datacenterLocation: "[ng-model='listingCtrl.computeServer.location']",
            gpuCount: "[ng-model='listingCtrl.computeServer.gpuCount']",
            usage: "[ng-model='listingCtrl.computeServer.cud']"
        }
        return this.rootEl.$(selectors[name])
    }

    selectSeries(name) {
        const selectors = {
            n1: "[value='n1']"
        }
        return $(selectors[name])
    }

    selectMachineType(name) {
        const selectors = {
            standard8: "[value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']"
        }
        return $(selectors[name])
    }

    selectLocation(name) {
        const selectors = {
            frankfurt: "[ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer'][value='europe-west3'] .md-text"
        }
        return $(selectors[name])
    }

    selectGPUType(name) {
        const selectors = {
            t4: "[value='NVIDIA_TESLA_T4'] .md-text"
        }
        return $(selectors[name])
    }

    selectGPUNumber(name) {
        const selectors = {
            one: "[ng-repeat='item in listingCtrl.supportedGpuNumbers[listingCtrl.computeServer.gpuType]'][value='1'] .md-text"
        }
        return $(selectors[name])
    }
    
    selectSSD(name) {
        const selectors = {
            two: "[ng-repeat='item in listingCtrl.dynamicSsd.computeServer'][value='2']"
        }
        return $(selectors[name])
    }
    
    selectUsage(name) {
        const selectors = {
            oneYear: "#select_option_138 .md-text"
        }
        return $(selectors[name])
    }

    
    get addGPUs() {
        return $("[ng-model='listingCtrl.computeServer.addGPUs']");
    }

    get addToEstimateBtn(){
        return $("button[ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']");
    }
}