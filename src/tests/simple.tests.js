import BasePage from "../po/pages/base.page.js";
import SearchResultPage from "../po/pages/searchResult.page.js";
import PricingCalculatorPage from "../po/pages/pricingCalculator.page.js";

const basePage = new BasePage();
const searchResultPage = new SearchResultPage();
const pricingCalculatorPage = new PricingCalculatorPage();
const ENTER = "\uE007";

describe('Google Cloud page', () => {

    beforeEach(async () => {
        await basePage.open();
    })

    it('Google calculator', async () => {
        await basePage.header.searchBtn.click();
        await basePage.header.searchBtn.setValue("Google Cloud Platform Pricing Calculator");
        await browser.keys(ENTER);
        await searchResultPage.pricingCalculatorLink.click();

        await browser.url("https://cloudpricingcalculator.appspot.com/"); //had some trouble finding the selectors after opening the calculator, so used this as a workaround
        await pricingCalculatorPage.calculatorHeader.computeEngineBtn.click();
        await pricingCalculatorPage.calculatorForm.input("noOfInstances").setValue("4");
        await pricingCalculatorPage.calculatorForm.input("series").click();
        await pricingCalculatorPage.calculatorForm.selectSeries("n1").click();
        await pricingCalculatorPage.calculatorForm.input("machineType").click();
        await pricingCalculatorPage.calculatorForm.selectMachineType("standard8").waitForDisplayed();
        await pricingCalculatorPage.calculatorForm.selectMachineType("standard8").click();
        await pricingCalculatorPage.calculatorForm.addGPUs.click();
        await pricingCalculatorPage.calculatorForm.input("gpuType").click();
        await pricingCalculatorPage.calculatorForm.selectGPUType("t4").waitForDisplayed();
        await pricingCalculatorPage.calculatorForm.selectGPUType("t4").click();
        await pricingCalculatorPage.calculatorForm.input("gpuCount").click();
        await pricingCalculatorPage.calculatorForm.selectGPUNumber("one").waitForDisplayed();
        await pricingCalculatorPage.calculatorForm.selectGPUNumber("one").click();
        await pricingCalculatorPage.calculatorForm.input("ssd").click();
        await pricingCalculatorPage.calculatorForm.selectSSD("two").waitForDisplayed();
        await pricingCalculatorPage.calculatorForm.selectSSD("two").click();
        await pricingCalculatorPage.calculatorForm.input("datacenterLocation").click();
        await pricingCalculatorPage.calculatorForm.selectLocation("frankfurt").waitForDisplayed();
        await pricingCalculatorPage.calculatorForm.selectLocation("frankfurt").click();
        await pricingCalculatorPage.calculatorForm.input("usage").click();
        await pricingCalculatorPage.calculatorForm.selectUsage("oneYear").waitForDisplayed();
        await pricingCalculatorPage.calculatorForm.selectUsage("oneYear").click();
        await pricingCalculatorPage.calculatorForm.addToEstimateBtn.click();

        // const text = await pricingCalculatorPage.calculatorResultBlock.cartTotal.getText();
        // await expect(text).toEqual("Total Estimated Cost: USD 1,840.40 per 1 month"); //was giving different cost results
        await expect(pricingCalculatorPage.calculatorResultBlock.cartTotal).toHaveTextContaining("Total Estimated Cost:");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("noOfInstances")).toHaveTextContaining("4");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("datacenterLocation")).toHaveTextContaining("Frankfurt");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("usage")).toHaveTextContaining("1 Year");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("model")).toHaveTextContaining("Regular");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("machineType")).toHaveTextContaining("n1-standard-8");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("os")).toHaveTextContaining("Free");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("gpuType")).toHaveTextContaining("1 NVIDIA TESLA T4");
        await expect(pricingCalculatorPage.calculatorResultBlock.output("ssd")).toHaveTextContaining("2x375");

    })

})